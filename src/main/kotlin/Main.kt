import utils.agreementSignatoryCountMapper
import utils.agreementSignatoryCountQuery
import utils.orgAverageSignatoryMapper
import utils.orgAverageSignatoryQuery
import java.sql.DriverManager
import utils.extensions.truncate

fun main(args: Array<String>) {
    // First argument is the db user (e.g. "lexadmin")
    // Second argument is the password (e.g. "BestProductEver!23")
    val jdbcUrl = "jdbc:postgresql://localhost:5432/corporate"
    val connection = DriverManager
        .getConnection(jdbcUrl, args[0], args[1])

    if (connection.isValid(0)) {
        return
    }

    connection.use {
        val orgAverageSignatoryResult = connection.prepareStatement(orgAverageSignatoryQuery).executeQuery()

        val agreementSignatoryCountResult = connection.prepareStatement(agreementSignatoryCountQuery).executeQuery()

        orgAverageSignatoryResult.use {
            // Transform query results to a map
            val orgDataViaQuery = orgAverageSignatoryMapper(orgAverageSignatoryResult).map {
                it.organizationId to it.avgNoOfSignatory
            }

            agreementSignatoryCountResult.use {

                /*
                Loop through all the records of agreement & signatory_count query
                and compute the average number of signatories per agreement per organization via Kotlin
                 */
                val agreementSignatoryRecords =
                    agreementSignatoryCountMapper(agreementSignatoryCountResult).groupBy { it.organizationId }

                // Transform results to a Map
                val orgDataViaCalculation = agreementSignatoryRecords.map {
                    (orgId, agreementSignatoryCounts) ->
                        val totalSignatory = agreementSignatoryCounts.sumOf { it.signatoryCount }
                        val averageSignatoryPerAgreement = (totalSignatory / agreementSignatoryCounts.count().toDouble()).truncate()
                        orgId to averageSignatoryPerAgreement
                }

                // Compare the results of query from EXER-002 and Kotlin-computed average number
                println(orgDataViaQuery.sortedBy { it.first })
                println(orgDataViaCalculation.sortedBy { it.first })

            }
        }
    }
}
