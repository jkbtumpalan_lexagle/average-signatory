package data

data class AgreementSignatoryCount(
    val organizationId: String?,
    val agreementId: String,
    val signatoryCount: Int,
)
