package data

data class OrgAverageSignatory(
    val organizationId: String?,
    val avgNoOfSignatory: Float,
)