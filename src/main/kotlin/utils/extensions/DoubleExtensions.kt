package utils.extensions

import kotlin.math.floor

// Extension function for double to truncate to first 2 decimals.
fun Double.truncate(): Double {
    return floor(this * 100) / 100
}