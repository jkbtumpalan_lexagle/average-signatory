package utils
import data.AgreementSignatoryCount
import data.OrgAverageSignatory
import java.sql.ResultSet

fun orgAverageSignatoryMapper(result: ResultSet): MutableList<OrgAverageSignatory> {
    val organizationAverageSignatoryData = mutableListOf<OrgAverageSignatory>()

    while (result.next()) {
        val organizationId: String? = result.getString("organization_id")
        val averageNoOfSignatory = result.getFloat("avg_no_of_signatory")

        organizationAverageSignatoryData.add(OrgAverageSignatory(organizationId, averageNoOfSignatory))
    }

    return organizationAverageSignatoryData
}

fun agreementSignatoryCountMapper(result: ResultSet): MutableList<AgreementSignatoryCount> {
    val agreementSignatoryCountData = mutableListOf<AgreementSignatoryCount>()

    while (result.next()) {
        val organizationId: String? = result.getString("organization_id")
        val agreementId = result.getString("agreement_id")
        val signatoryCount = result.getInt("signatory_count")

        agreementSignatoryCountData.add(AgreementSignatoryCount(organizationId, agreementId, signatoryCount))
    }

    return agreementSignatoryCountData
}
