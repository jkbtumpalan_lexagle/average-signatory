package utils

const val orgAverageSignatoryQuery =
    """
        SELECT a.organization_id,
            TRUNC(
                COUNT(aas.signatory_id)::decimal /
                COUNT(DISTINCT(aas.agreement_id)), 2
            ) as avg_no_of_signatory
        FROM "agreement".agreement_signatory as aas
            LEFT JOIN "user".account as a
                on aas.added_by = a.email
            GROUP BY a.organization_id
    """

const val agreementSignatoryCountQuery =
    """
         SELECT a.organization_id,
            aas.agreement_id, 
            COUNT(aas.signatory_id) as signatory_count
         FROM "agreement".agreement_signatory as aas
            LEFT JOIN "user".account as a
                ON aas.added_by = a.email
         GROUP BY a.organization_id, aas.agreement_id
    """
